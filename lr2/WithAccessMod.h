#include <iostream>
#include <string>
#pragma once
using namespace std;

namespace WithAccessMod {
	class Point {
	private:
		string someInnerState;
	protected:
		int x;
		int y;
	public:
		Point() {
			cout << "Point()\n";
			x = 0;
			y = 0;
			someInnerState = "Initialized";
		}

		void setX(int x) {
			cout << "Point::setX()\n";
			this->x = x;
			someInnerState = "LastMutationX";
		}

		int getX() { return x; }

		void setY(int y) {
			cout << "Point::setY()\n";
			this->y = y;
			someInnerState = "LastMutationX";
		}

		int getY() { return y; }
	};

	class Point3D : public Point {
	private:
		string someInnerState;

	protected:
		int z;

	public:
		Point3D() {
			cout << "Point3D()\n";
			z = 0;
			someInnerState = "Initialized";
		}

		void setZ(int z) {
			cout << "Point3D::setZ()\n";
			this->z = z;
			someInnerState = "LastMutationZ";
		}

		int getZ() { return z; }

		void printCoords() {
			cout << x << " " << y << " " << z << endl;
		}
	};

	void run() {
		Point p = Point();
		// p: someInnerState is inaccessible because it's private, x and y are protected
		p.setX(20);
		p.setY(40);
		cout << p.getX() << " " << p.getY() << endl;

		Point3D p3d = Point3D();
		// p3d: someInnerState is inaccessible because it's private, x, y and z are protected
		p3d.setX(1246);
		p3d.setY(3486);
		p3d.setZ(2387);
		cout << p3d.getX() << " " << p3d.getY() << " " << p3d.getZ() << endl;
		p3d.printCoords();

	}
}
