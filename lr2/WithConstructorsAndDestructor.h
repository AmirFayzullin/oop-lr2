#include <iostream>
#include <cstdio>
#pragma once
using namespace std;

namespace WithCAndD {
	class Point {
	public:
		int x;
		int y;

		Point() {
			cout << "Point()\n";
			x = 0;
			y = 0;
		}

		Point(int x) {
			printf("Point(%d)\n", x);
			this->x = x;
			y = 0;
		}

		Point(int x, int y) {
			printf("Point(%d, %d)\n", x, y);
			this->x = x;
			this->y = y;
		}

		Point(Point& p) {
			cout << "Point(Point& p)\n";
			x = p.x;
			y = p.y;
		}

		~Point() {
			cout << "~Point() " << x << " " << y << endl;
		}

		void setX(int x) {
			printf("Point::setX(%d)\n", x);
			this->x = x;
		}

		void setY(int y) {
			printf("Point::setY(%d)\n", y);
			this->y = y;
		}
	};

	void run() {
		Point* p = new Point();
		Point p2(11);
		Point p3(10, 20);
		Point p4(p3);
		cout << p3.x << " " << p3.y << endl;
		cout << p4.x << " " << p4.y << endl;

		delete p;
	}
}
