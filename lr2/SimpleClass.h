#include <iostream>
#include <cstdio>
#pragma once
using namespace std;

namespace SimpleClass {
	class Point {
	public:
		int x;
		int y;

		void setX(int x) {
			cout << "Point::setX() " << x << endl;
			this->x = x;
		}

		void setY(int y) {
			cout << "Point::setY() " << y << endl;
			this->y = y;
		}

		void printCoords();
	};

	void Point::printCoords() {
		printf("Point::printCoords()\nx: %d, y: %d\n", x, y);
	}


	void run() {
		Point p;
		p.setX(10);
		p.setY(20);
		cout << p.x << endl;
		cout << p.y << endl;
		p.printCoords();

		Point* p2 = new Point;
		p2->setX(45);
		p2->setY(86);
		cout << p2->x << endl;
		cout << p2->y << endl;

		delete p2;
	}
}
