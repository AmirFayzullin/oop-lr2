#include <iostream>
#include <cstdio>
#include <string>
#pragma once
using namespace std;

namespace WithInheritance {
	class Point {
	public:
		int x;
		int y;

		Point() {
			cout << "Point()\n";
			x = 0;
			y = 0;
		}

		Point(int x) {
			printf("Point(%d)\n", x);
			this->x = x;
			y = 0;
		}

		Point(int x, int y) {
			printf("Point(%d, %d)\n", x, y);
			this->x = x;
			this->y = y;
		}

		Point(Point& p) {
			cout << "Point(Point& p)\n";
			x = p.x;
			y = p.y;
		}

		~Point() {
			printf("~Point() %d %d\n", x, y);
		}

		void setX(int x) {
			printf("Point::setX(%d)\n", x);
			this->x = x;
		}

		void setY(int y) {
			printf("Point::setY(%d)\n", y);
			this->y = y;
		}

		void printCoords() {
			printf("Point::printCoords()\nx: %d, y: %d\n", x, y);
		}

		virtual void sayHi() {
			cout << "Hi! I'm a Point instance!\n";
		}
	};

	class Point3D : public Point {
	public:
		int z;

		Point3D() {
			cout << "Point3D()\n";
			z = 0;
		}

		Point3D(int z) {
			printf("Point3D(%d)\n", z);
			this->z = z;
		}

		Point3D(int x, int y, int z) : Point(x, y) {
			printf("Point3D(%d, %d, %d)\n", x, y, z);
			this->z = z;
		}

		Point3D(Point3D& p) : Point(p) {
			cout << "Point3D(Point3D& p)\n";
			z = p.z;
		}

		~Point3D() {
			printf("~Point3D() %d %d %d\n", x, y, z);
		}

		void setZ(int z) {
			printf("Point3D::setZ(%d)\n", z);
			this->z = z;
		}

		// method redeclaration
		void printCoords() {
			printf("Point3D::printCoords()\nx: %d, y: %d, z: %d\n", x, y, z);
		}

		// method overriding
		void sayHi() override {
			cout << "Hi! I'm a Point3D instance!\n";
		}
	};

	void run() {
		Point3D p(10, 20, 30);
		Point3D p2(p);

		p.printCoords();
		p2.printCoords();

		Point* p3 = new Point3D();
		//setZ() and z are inaccessible in p3

		p3->setX(20);
		p3->printCoords();	// called parent method, because p3 is stored in Point var
		p3->sayHi();		// overriding method call

		delete p3;
		// ~Point3D won't be called, because p3 is stored in its' base class variable
		// and destructor of the base class isn't virtual
	}
}
