#include <iostream>
#include <cstdio>
#pragma once
using namespace std;

namespace Composition {
	class Point {
	public:
		int x;
		int y;

		Point() {
			cout << "Point()\n";
			x = 0;
			y = 0;
		}

		Point(int x, int y) {
			printf("Point(%d, %d)\n", x, y);
			this->x = x;
			this->y = y;
		}

		~Point() {
			printf("~Point() %d %d \n", x, y);
		}
	};

	class Section {
		Point p1;		// created Point with default constructor
		Point* p2;
	public:
		Section(int x, int y) : p1() {
			printf("Section(%d, %d)\n", x, y);
			//p1 = Point();				// created new Point and previous is deleted
			p2 = new Point(x, y);		
		}

		Section(int x1, int y1, int x2, int y2) : p1(x1, x2) {
			printf("Section(%d, %d, %d, %d)\n", x1, y1, x2, y2);
			//p1 = Point(x1, y1);
			p2 = new Point(x2, y2);
		}

	

		~Section() {
			cout << "~Section()\n";
			//Point p1 is deleted automatically 
			delete p2;
		}
	};

	void run() {
		Section s = Section(10, 20);
	}
}
