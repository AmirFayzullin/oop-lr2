#include <iostream>
#include "SimpleClass.h"
#include "WithConstructorsAndDestructor.h"
#include "WithInheritance.h"
#include "WithAccessMod.h"
#include "Composition.h"
using namespace std;

typedef void (*PF)();

int main() {
	PF menu[5] = { SimpleClass::run, WithCAndD::run, WithInheritance::run, WithAccessMod::run, Composition::run };
	
	while (true) {
		cout << "\n\n1) SimpleClass\n" 
			<< "2) With constructors and destructors\n" 
			<< "3) With inheritance\n" 
			<< "4) With access modifiers\n" 
			<< "5) Exit\n" 
			<< ">> ";
		int option;
		cin >> option;
		option--;
		if (option == 5) break;
		else if (option >= 0 && option < 5) {
			printf("\n\n");
			menu[option]();
		}
	}
}